<?php

// $Id$

/*
 * @file
 * Administration page callbacks for the Embargo module
 */

/*
 * Form builder. Configure Embargo
 *
 * @ingroup forms
 */
function embargo_admin_settings() {
  $form = array();

  //Get a list (array) of nodetypes.
  $options = node_get_types('names');

  $form['embargo_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('The following content types can be put under embargo'),
    '#options' => $options,
    '#default_value' => variable_get('embargo_node_types', array()),
    '#description' => t('A checkbox will be available on these nodes to put them under embargo.'),
  );

  $date_format = 'Y-m-d H:i';
  $default_date = format_date(time(), 'custom', $date_format);

  $form['embargo_enddate'] = array(
    '#type' => 'date_select',
    '#title' => t('Enddate for the embargo'),
    '#default_value' => variable_get('embargo_enddate', $default_date),
    '#date_format' => $date_format,
    '#date_label_position' => 'within',
  );

  return system_settings_form($form);
}